import 'package:flutter/material.dart';
import 'package:why_flutter_examples/1_user_interface/1_1_user_card.dart';
import 'package:why_flutter_examples/1_user_interface/1_2_user_card_reordered.dart';
import 'package:why_flutter_examples/model/user_model.dart';

List<UserModel> listItems = [
    UserModel("https://randomuser.me/api/portraits/women/28.jpg", "Michele", "Webb"),
    UserModel("https://randomuser.me/api/portraits/women/27.jpg", "Marion", "Hamilton"),
    UserModel("https://randomuser.me/api/portraits/women/26.jpg", "Alma", "Romero"),
    UserModel("https://randomuser.me/api/portraits/women/25.jpg", "Sofia", "Henderson"),
    UserModel("https://randomuser.me/api/portraits/women/24.jpg", "Andrea", "Hawkins"),
    UserModel("https://randomuser.me/api/portraits/women/23.jpg", "Arianna", "Bishop"),
    UserModel("https://randomuser.me/api/portraits/women/22.jpg", "Kenzi", "Sutton"),
    UserModel("https://randomuser.me/api/portraits/women/21.jpg", "Genesis", "Collins"),
    UserModel("https://randomuser.me/api/portraits/women/20.jpg", "Robin", "Stevens")
];

class UserListDynamic extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.all(5.0),
      itemCount: listItems.length,
      itemBuilder: (context, position) {
        return UserCard(listItems[position]);
      }
    );
  }
}