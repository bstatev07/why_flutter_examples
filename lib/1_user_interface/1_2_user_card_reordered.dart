import 'package:flutter/material.dart';
import 'package:why_flutter_examples/model/user_model.dart';

class UserCardReordered extends StatelessWidget {
  final UserModel userModel;

  UserCardReordered(this.userModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
      margin: EdgeInsets.symmetric(vertical: 5),
      decoration:
          BoxDecoration(border: Border.all(color: Color(0xFFdff2df), width: 3)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          NamesColumn(
              firstName: userModel.firstName, lastName: userModel.lastName),
          Stack(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(5),
                  child: Image.network(userModel.imgUrl, height: 80)),
              Positioned(right: 0.0, top: 0.0, child: Icon(Icons.new_releases, color: Colors.blue)),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.favorite_border, color: Colors.black),
                onPressed: () {},
              ),
              FlatButton(
                child: Text("0"),
                onPressed: () {},
              )
            ],
          ),
        ],
      ),
    );
  }
}

class NamesColumn extends StatelessWidget {
  const NamesColumn({
    Key key,
    @required this.firstName,
    @required this.lastName,
  }) : super(key: key);

  final String firstName;
  final String lastName;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(firstName,
            style: TextStyle(fontSize: 16.0, fontFamily: "Helvetica")),
        SizedBox(
          height: 12.0,
        ),
        Text(lastName,
            style: TextStyle(fontSize: 20.0, fontFamily: "Helvetica")),
      ],
    );
  }
}
