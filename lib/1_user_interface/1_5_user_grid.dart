import 'package:flutter/material.dart';
import 'package:why_flutter_examples/1_user_interface/1_1_user_card.dart';
import 'package:why_flutter_examples/model/user_model.dart';

class UserGrid extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 3,
      childAspectRatio: 1.5,
      mainAxisSpacing: 10.0,
      crossAxisSpacing: 10.0,
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      primary: false,
      padding: const EdgeInsets.all(5.0),
      children: <Widget>[
        UserCard(UserModel("https://randomuser.me/api/portraits/women/28.jpg", "Michele", "Webb")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/27.jpg", "Marion", "Hamilton")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/26.jpg", "Alma", "Romero")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/25.jpg", "Sofia", "Henderson")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/24.jpg", "Andrea", "Hawkins")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/23.jpg", "Arianna", "Bishop")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/22.jpg", "Kenzi", "Sutton")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/21.jpg", "Genesis", "Collins")),
        UserCard(UserModel("https://randomuser.me/api/portraits/women/20.jpg", "Robin", "Stevens")),
      ],
    );
  }
}