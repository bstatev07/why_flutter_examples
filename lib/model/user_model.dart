class UserModel {
  final String imgUrl;
  final String firstName;
  final String lastName;

  UserModel(this.imgUrl, this.firstName, this.lastName);
}