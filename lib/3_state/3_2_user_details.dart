import 'package:flutter/material.dart';
import 'package:why_flutter_examples/3_state/3_1_user_card.dart';
import 'package:why_flutter_examples/model/user_model.dart';

class UserDetails extends StatefulWidget{
  final UserModel userModel;

  UserDetails(this.userModel);

  State<StatefulWidget> createState()
  {
    return UserDetailsState(userModel);
  }
}

class UserDetailsState extends State<UserDetails>  {
  final UserModel userModel;
  int points = 0;

  UserDetailsState(this.userModel);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Points +$points"),
      ),
      body: Column(children: <Widget>[
          UserCard(this)
      ],),
    );
  }
}