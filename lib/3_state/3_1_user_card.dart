import 'package:flutter/material.dart';
import 'package:why_flutter_examples/3_state/3_2_user_details.dart';

class UserCard extends StatelessWidget {
  final UserDetailsState parent;

  UserCard(this.parent);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
        margin: EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            border:
                Border.all(color: Theme.of(context).primaryColor, width: 2)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Image.network(this.parent.userModel.imgUrl, height: 90),
            Expanded(
                child: NamesColumn(
                    firstName: this.parent.userModel.firstName,
                    lastName: this.parent.userModel.lastName)),
            ButtonsColumn(parent: this.parent),
          ],
        ));
  }
}

class ButtonsColumn extends StatefulWidget {
  final UserDetailsState parent;

  ButtonsColumn({@required this.parent});

  State<StatefulWidget> createState() {
    return _ButtonsColumnState(parent: parent);
  }
}

class _ButtonsColumnState extends State<ButtonsColumn> {
  _ButtonsColumnState({@required this.parent}) : super();

  UserDetailsState parent;
  bool isFavorite = false;

  @override
  initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
          icon: Icon(isFavorite ? Icons.favorite : Icons.favorite_border,
              color: isFavorite ? Colors.red : Colors.black),
          onPressed: () {
            setState(() {
              isFavorite = !isFavorite;
            });
          },
        ),
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            this.parent.setState(() {
              this.parent.points++;
            });
          },
        )
      ],
    );
  }
}

class NamesColumn extends StatelessWidget {
  const NamesColumn({
    Key key,
    @required this.firstName,
    @required this.lastName,
  }) : super(key: key);

  final String firstName;
  final String lastName;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(firstName,
            style: TextStyle(
                color: Color(0xFF3e603a),
                fontSize: 16.0,
                fontFamily: "Helvetica")),
        SizedBox(
          height: 12.0,
        ),
        Text(lastName,
            style: TextStyle(fontSize: 20.0, fontFamily: "Helvetica")),
      ],
    );
  }
}
