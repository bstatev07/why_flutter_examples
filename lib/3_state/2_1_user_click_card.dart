import 'package:flutter/material.dart';
import 'package:why_flutter_examples/model/user_model.dart';
import 'package:why_flutter_examples/1_user_interface/1_1_user_card.dart';
import 'package:why_flutter_examples/3_state/3_2_user_details.dart';

class UserClickCard extends StatelessWidget {
  final UserModel userModel;

  UserClickCard(this.userModel);

  navigateToDetails(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => (UserDetails(userModel))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => navigateToDetails(context),
        child: UserCard(userModel)
    );
  }
}