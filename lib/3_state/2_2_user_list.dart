import 'package:flutter/material.dart';
import 'package:why_flutter_examples/model/user_model.dart';
import 'package:why_flutter_examples/3_state/2_1_user_click_card.dart';

class UserList extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      primary: false,
      padding: const EdgeInsets.all(5.0),
      children: <Widget>[
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/28.jpg", "Michele", "Webb")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/27.jpg", "Marion", "Hamilton")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/26.jpg", "Alma", "Romero")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/25.jpg", "Sofia", "Henderson")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/24.jpg", "Andrea", "Hawkins")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/23.jpg", "Arianna", "Bishop")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/22.jpg", "Kenzi", "Sutton")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/21.jpg", "Genesis", "Collins")),
        UserClickCard(UserModel("https://randomuser.me/api/portraits/women/20.jpg", "Robin", "Stevens")),
      ],
    );
  }
}