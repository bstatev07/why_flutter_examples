import 'package:flutter/material.dart';
import 'package:why_flutter_examples/model/user_model.dart';
import 'package:why_flutter_examples/1_user_interface/1_1_user_card.dart';

class UserDetails extends StatelessWidget {
  final UserModel userModel;

  UserDetails(this.userModel);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("More details"),
      ),
      body: Column(children: <Widget>[
          UserCard(userModel),
          MaterialButton(child: Text('Like'), onPressed: (){Navigator.pop(context, "You liked " + userModel.firstName);}),
          MaterialButton(child: Text('Dislike'), onPressed: (){Navigator.pop(context, "You disliked " + userModel.firstName);})
      ],),
    );
  }
}