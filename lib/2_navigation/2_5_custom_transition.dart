import 'package:flutter/material.dart';
import 'package:why_flutter_examples/2_navigation/2_1_user_click_card.dart';
import 'package:why_flutter_examples/2_navigation/2_3_user_details.dart';

navigateWithCustomTransition(BuildContext context, UserClickCard userCard) async {
    Navigator.push(
      context,
      SlideRightRoute(widget: UserDetails(userCard.userModel))
    );
  }

class SlideRightRoute extends PageRouteBuilder {
  final Widget widget;
  SlideRightRoute({this.widget})
      : super(
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
        return widget;
      },
      transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
        return new SlideTransition(
          position: new Tween<Offset>(
            begin: const Offset(0.9, 0.9),
            end: Offset.zero,
          ).animate(animation),
          child: child,
        );
      }
  );
}