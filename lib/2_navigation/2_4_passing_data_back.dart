import 'package:flutter/material.dart';
import 'package:why_flutter_examples/2_navigation/2_1_user_click_card.dart';
import 'package:why_flutter_examples/2_navigation/2_3_user_details.dart';

navigateAndDisplayChoice(BuildContext context, UserClickCard userCard) async {
  final result = await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => (UserDetails(userCard.userModel))),
  );

  if (result != null) {
    Scaffold.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text("$result")));
  }
}