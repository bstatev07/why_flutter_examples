import 'package:flutter/material.dart';
import 'package:why_flutter_examples/model/user_model.dart';
import 'package:why_flutter_examples/1_user_interface/all.dart';
//import 'package:why_flutter_examples/2_navigation/all.dart';
//import 'package:why_flutter_examples/3_state/all.dart';

void main() => runApp(MaterialApp(
    home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(title: Text(("New Faces App"))),
        //body: UserCard(UserModel("https://randomuser.me/api/portraits/women/28.jpg", "Michele", "Webb"))
        body: UserList()
    ),
    routes: {

    },
    debugShowCheckedModeBanner: false
));
